import SocketServer
import socket
import unittest
import threading

import pop3srv

HOST = '127.0.0.1'
PORT = 1110


class TestServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    allow_reuse_address = True


class MyRequestHandlerTest(unittest.TestCase):
    def setUp(self):
        self.server = TestServer((HOST, PORT), pop3srv.POP3Handler)
        self.serverallow_reuse_address = True
        self.server.users = pop3srv.load_json("users.json")
        self.server.messages = pop3srv.load_json("messages.json")
        self.server_thread = threading.Thread(target=self.server.serve_forever)
        self.server_thread.start()
        self.client = socket.create_connection((HOST, PORT))

    def tearDown(self):
        self.client.close()
        self.server.shutdown()
        self.server.server_close()

    def test_user(self):
        self.client.send('USER unknown_user\n')
        result = self.client.recv(1024)
        self.assertEqual('ERR\n', result)

        self.client.send('USER user1\n')
        result = self.client.recv(1024)
        self.assertEqual('OK\n', result)

    def test_pass(self):
        self.client.send('USER user1\n')
        result = self.client.recv(1024)
        self.assertEqual('OK\n', result)

        self.client.send('PASS password2\n')
        result = self.client.recv(1024)
        self.assertEqual('ERR\n', result)

        self.client.send('PASS password1\n')
        result = self.client.recv(1024)
        self.assertEqual('OK\n', result)

    def test_session(self):
        self.client.send('USER user1\n')
        result = self.client.recv(1024)
        self.assertEqual('OK\n', result)

        self.client.send('PASS password1\n')
        result = self.client.recv(1024)
        self.assertEqual('OK\n', result)

        expected = """You've got 3 letters in your mailbox
1       Subject 1
2       Email 2
3       Hello world
Feel free to fetch any with RETR command
"""
        self.client.send('LIST\n')
        result = self.client.recv(1024)
        self.assertEqual(expected, result)

        expected = """Subject: Email 2
Email 2 body.

"""
        self.client.send('RETR 2\n')
        result = self.client.recv(1024)
        self.assertEqual(expected, result)

        self.client.send('QUIT\n')
        result = self.client.recv(1024)
        self.assertEqual("Bye!\n", result)

if __name__ == '__main__':
    unittest.main()
