"""
POP3 server

author: Fabien Schwob
"""
import SocketServer
import json
import logging


def load_json(filename):
    with open(filename, 'r') as handler:
        return json.load(handler)
    return {}


class POP3Handler(SocketServer.StreamRequestHandler):
    """
    Handle a POP3 connection
    """
    commands = {
        'USER': 'handle_user',
        'PASS': 'handle_pass',
        'LIST': 'handle_list',
        'RETR': 'handle_retr',
        'QUIT': 'handle_quit',
    }

    def message(self, stream, message_type, msg=None):
        """
        Return a message
        """
        if msg is not None and len(msg) > 0:
            stream.write("{0} {1}\n".format(message_type, msg))
        else:
            stream.write("{0}\n".format(message_type))

    def raw_message(self, stream, msg):
        """
        Return a raw message
        """
        stream.write("{0}\n".format(msg))

    def ok_message(self, stream, msg=None):
        """
        Return an OK message
        """
        self.message(stream, "OK", msg)

    def err_message(self, stream, msg=None):
        """
        Return an ERR message
        """
        self.message(stream, "ERR", msg)

    def handle_user(self, args, stream):
        """
        Handle USER command
        """
        if len(args) > 1:
            self.err_message(stream, "USER only takes one arg")
        else:
            if args[0] in self.server.users:
                self.username = args[0]
                self.ok_message(stream)
            else:
                self.username = None
                self.err_message(stream)

    def handle_pass(self, args, stream):
        """
        Handle USER command
        """
        if len(args) > 1:
            self.err_message(stream, "PASS only takes one arg")
        elif self.username is None:
            self.err_message(stream, "PASS must be called after USER command")
        else:
            expected_password = self.server.users[self.username]
            if expected_password == args[0]:
                self.is_auth = True
                self.ok_message(stream)
            else:
                self.err_message(stream)

    def handle_list(self, args, stream):
        """
        Handle LIST command
        """
        if len(args) > 0:
            self.err_message(stream, "LIST take no args")
        elif not self.is_auth:
            self.err_message(stream, "must be authenticated to call LIST")
        else:
            user_messages = self.server.messages.get(self.username, [])
            msgs = []
            msgs_count = len(user_messages)
            msgs.append(
                "You've got {0} letters in your mailbox".format(msgs_count)
            )
            for index, user_message in enumerate(user_messages, 1):
                msg_subject = user_message['subject']
                msgs.append("{0}       {1}".format(index, msg_subject))
            msgs.append("Feel free to fetch any with RETR command")
            self.raw_message(stream, "{0}".format("\n".join(msgs)))

    def handle_retr(self, args, stream):
        """
        Handle RETR command
        """
        if len(args) > 1:
            self.err_message(stream, "RETR take one arg")
        elif not self.is_auth:
            self.err_message(stream, "must be authenticated to call RETR")
        else:
            user_messages = self.server.messages.get(self.username, [])
            try:
                user_message = user_messages[int(args[0])-1]
                msgs = []
                msgs.append("Subject: {0}".format(user_message['subject']))
                msgs.append("{0}\n".format(user_message['body']))
                self.raw_message(stream, "{0}".format("\n".join(msgs)))
            except IndexError:
                self.err_message(stream, "no message with index {0}".format(args[0]))
            except ValueError:
                self.err_message(stream, "message number must be integer")

    def handle_quit(self, args, stream):
        """
        Handle QUIT command
        """
        self.quit = True
        self.raw_message(stream, "Bye!")

    def handle(self):
        try:
            self.quit = False
            self.username = None
            self.is_auth = False
            while self.quit is False:
                command_elements = self.rfile.readline().strip().split(" ")
                command = command_elements[0]
                args = command_elements[1:]
                if command in self.commands:
                    current_command = getattr(self, self.commands[command], None)
                    if current_command is not None:
                        current_command(args, self.wfile)
                    else:
                        msg = "command not implemented: {0}\n".format(command)
                        self.err_message(self.wfile, msg)
                        self.wfile.write(msg)
                else:
                    msg = "invalid command: {0}\n".format(command.upper())
                    self.err_message(self.wfile, msg)
        except Exception, e:
            logging.info("Error while processing commands")
if __name__ == "__main__":
    host = "localhost"
    port = 1110

    # Create the server, binding to localhost on port 9999
    server = SocketServer.TCPServer((host, port), POP3Handler)
    server.allow_reuse_address = True
    server.users = load_json("users.json")
    server.messages = load_json("messages.json")

    # Launch the server until you Ctrl-C is pressed
    server.serve_forever()
